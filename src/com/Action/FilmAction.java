package com.Action;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import com.Model.Film;
import com.Model.Manager;
import com.opensymphony.xwork2.ActionSupport;


/**
 * Clase acci�n  encargada de procesar la informaci�n 
 *   con respecto a los films.
 * 
 */
@SuppressWarnings("serial")
public class FilmAction extends ActionSupport implements ServletRequestAware{
	
	private Iterable<Film>  films;
	private Film film;
	private String idFilm;
	private	HttpServletRequest request;
	private Manager log; 
	
	/**
	 * Constructor que instancia a log; variable 
	 * que denota un objeto de clase Manager.
	 */

	public FilmAction(){
		
		log = new Manager();
	}
	
	/**
	 * Al ejecutarse carga la lista de films.
	 */
	
	public String execute() throws Exception{
		
		this.setFilms(log.getFilms());
		return "success";
	}
	
	/**
	 * 
	 * @return film actual.
	 */

	public Film getFilm() {
		return film;
	}
	/**
	 * Establece el film
	 * @param film
	 */

	public void setFilm(Film film) {
		this.film = film;
	}
	
	/**
	 * Elimina el film de acuerdo a su id.
	 * @return SUCCESS en caso de haber salido bien. 
	 * @throws Exception
	 */
	
	public String delete() throws Exception{		
		log.remove(idFilm);
		return SUCCESS;
	}
	
	/**
	 * Estabablece el Film de a cuerdo a su id.
	 * @return
	 * @throws Exception
	 */
	
	public String showFilm() throws Exception{			
		film = log.getFilm(this.idFilm);	
		return SUCCESS;	
	}
	
	/**
	 * Si el usuario esta registrado entonces carga un film.
	 * @return
	 * @throws Exception
	 */
	public String edit()throws Exception{		
		String r;		
		HttpServletRequest request = ServletActionContext.getRequest();
		if(request.getSession().getAttribute("validacionAdmin")==null)
			r=ERROR;
		else
			r = this.showFilm();		
		
		return r;	
	}
	/**
	 * Devuelve una cadena con el id del film.
	 * @return
	 */

	public String getIdFilm() {
		return idFilm;
	}
	/**
	 * Establece el id del Film.
	 * @param idFilm
	 */

	public void setIdFilm(String idFilm) {
		this.idFilm = idFilm;
	}
	
	/**
	 * Devuelve la coleccion iterable de films.
	 * @return
	 */

	public Iterable<Film> getFilms() {
		return films;
	}
	
	/**
	 * Establece la colecci�n iterable de films.
	 * @param films
	 */

	public void setFilms(Iterable<Film> films) {
		this.films = films;
	}
	
	/**
	 * Establece un servletRequest.
	 */

	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	/**
	 * Retorna Serlvet Request
	 * @return
	 */
 
	public HttpServletRequest getServletRequest() {
		return this.request;
	}
}
