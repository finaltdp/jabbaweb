package com.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
/**
 * Clase Encargada de procesar la informaci�n de los formularios de registro del usuario.
 *
 */
@SuppressWarnings("serial")
public class Login extends ActionSupport  {
	
	private String username;
	private String password;
    HttpSession sesion;
    
    /**
     * Carga un usuario en la sesi�n.
     */
    
    @Override
    public String execute() throws Exception{
    	HttpServletRequest request = ServletActionContext.getRequest();
        sesion = request.getSession();
        sesion.removeAttribute("validacionAdmin");
        sesion.setAttribute("validacionAdmin", true);
        return SUCCESS;
    }
    
    
    
	/**
	 * Valida el usuario y la contrase�a
	 */
	@Override
	public void validate(){ //Struts valida antes de execute().    
        if(!username.equals("admin"))
        	addFieldError("username", "Usuario invalido");
        else if (!password.equals("admin"))
        		addFieldError("password", "Contrase�a invalida");
	}
	/**
	 * setea el username.
	 * @param username
	 */
	
	//@RequiredStringValidator(message="Username obligatorio.")
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * setea el password
	 * @param password
	 */
	
	//@RequiredStringValidator(message="Password obligatorio.")
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * Retorna la sesion.
	 * @return
	 */
	
	public HttpSession getSesion() {
		return sesion;
	}
	/**
	 * Setea la sesion.
	 * @param sesion
	 */
	public void setSesion(HttpSession sesion) {
		this.sesion = sesion;
	}
	/**
	 * Devuelve el usuario.
	 * @return
	 */
		
	public String getUsername() {
		return username;
	}
	/**
	 * Devuelve el password.
	 * @return
	 */
	
	public String getPassword() {
		return password;
	}
}
