package com.Action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.Model.Film;
import com.Model.Manager;
import com.opensymphony.xwork2.ActionSupport;
/**
 * Clase encargada de procesar la información
 * subministrada por el formulario de Registro
 * con respecto a los nuevos Films creados
 * o a los que son Editados.
 * 
 *
 */
@SuppressWarnings("serial")
public class Register extends ActionSupport{
	
	private Film filmBean;
	private String oldTitle;
	private Manager log;
	/**
	 * Constructor que instancia a log; variable 
	 * que denota un objeto de clase Manager.
	 */
	public Register(){
		log = new Manager();
	}
	
	/**
	 * Si fue seleccionado un film 
	 * lo remueve 
	 * inserta el nuevo film
	 * 
	 */
    @Override
    public String execute() throws Exception {         
        //call Service class to store personBean's state in database   
    	if(oldTitle!=null)
    		log.remove(oldTitle);
    	
    	log.add(filmBean);
    	return SUCCESS;
    }
    /**
     * Reglas de validación para la inseción del nuevo film.
     */
    
    public void validate(){
    	HttpServletRequest request = ServletActionContext.getRequest();
    	if(request.getSession().getAttribute("validacionAdmin")==null)
    		addFieldError( "filmBean.title", "Solo el admin puede relizar cambios." ); 
    		//Al no validar no se ejecuta el execute. Pero "devuelve" "input".
    	if (!validRate(filmBean.getRate()))
    		addFieldError( "filmBean.rate", "Ingrese un valor entre 1 y 5" );
    }

	private boolean validRate(String rate) {
		return rate.equals("1")||rate.equals("2")||rate.equals("3")||
				rate.equals("4")||rate.equals("5");
	}
	/**
	 * Devuelve el Film.
	 * @return
	 */

	public Film getFilmBean() {
		return filmBean;
	}
	
	/**
	 * Setea un film.
	 * @param filmBean
	 */

	public void setFilmBean(Film filmBean) {
		this.filmBean = filmBean;
	}
	/**
	 * Retorna el viejo título.
	 * @return
	 */

	public String getOldTitle() {
		return oldTitle;
	}
	/**
	 * Setea el viejo titulo.
	 * @param oldTitle
	 */

	public void setOldTitle(String oldTitle) {
		this.oldTitle = oldTitle;
	}
}
