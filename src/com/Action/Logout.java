package com.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import com.opensymphony.xwork2.ActionSupport;
/**
 * Clase encargada de Manipular el cierre de session.
 *
 */
@SuppressWarnings("serial")
public class Logout extends ActionSupport{
	
	/**
     * Cierra la sesi�n.
     * @return
     * @throws Exception
     */
        
	@Override
    public String execute() throws Exception{
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpSession sesion = request.getSession();
        sesion.removeAttribute("validacionAdmin");
        return SUCCESS;
    }
}
