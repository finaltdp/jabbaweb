package com.Model;
/**
 * Clase Film correspondiente
 * a capturar las carateristicas
 * de los films 
 *
 */
@SuppressWarnings("serial")
public class Film  implements java.io.Serializable{
	
	private String title;
	private String opinion;
	private String rate;
	private String IMDb;
	private String trailer;
	/**
	 * retorna el titulo 
	 * @return
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * setea el titulo
	 * @param title
	 */
	
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * retorna la opinion
	 * @return
	 */
	
	public String getOpinion() {
		return opinion;
	}
	/**
	 * setea la opinion 
	 * @param opinion
	 */
	public void setOpinion(String opinion) {
		this.opinion = opinion;
	}
	/**
	 * establece la puntuacion.
	 * @return
	 */
	public String getRate() {
		return rate;
	}
	/**
	 * retorna la puntuacion.
	 * @param rate
	 */
	
	public void setRate(String rate) {
		this.rate = rate;
	}
	/**
	 * redefine el metodo toString,
	 * devuelve el titulo.
	 */
	public String toString(){
		String toR = this.getTitle();
		/*
		toR += "Opinion : "+ this.opinion +"\n";
		toR +="Rate : " + this.rate;
		*/
		return toR;
	}
	/**
	 * devuelve el link a la fiche de IMDb.
	 * @return
	 */
	
	public String getIMDb() {
		return IMDb;
	}
	
	/**
	 * Se espera link a IMDb de la forma ej:
	 * https://www.imdb.com/ si no simplemente lo hace null.
	 * @param IMDb link a IMDb.
	 */
	public void setIMDb(String IMDb) {	
		if (IMDb!=null && IMDb.contains("imdb.com/"))
			this.IMDb=IMDb;
		else
			this.IMDb=null;
	}
	/**
	 * Devuelve el link con el trailer a youtube.
	 * @return
	 */

	public String getTrailer() {
		return trailer;
	}
	
	/**
	 * Setea el trailer del link con un video de YouTube se espera la forma ej:
	 * https://www.youtube.com/watch?v=PulHR2LfPVk si no simplemente lo hace null.
	 * @param trailer link al trailer en YouTube.
	 */
	public void setTrailer(String trailer) {
		if (trailer.contains("embed"))//Implica ya se carg� una vez.
			this.trailer=trailer;
		else{
			if (trailer.contains("youtube.com/watch?v=")){
				String[] a=trailer.split("="); //Se obtiene YouTube ID
				this.trailer = "http://www.youtube.com/embed/"+a[a.length-1];
			}else
				this.trailer=null;			
		}
	}
}
