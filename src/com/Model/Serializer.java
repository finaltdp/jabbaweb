package com.Model;

import java.io.*;
import java.util.HashMap;
/**
 * C�ase encargada de la Serializaci�n con metodos est�ticos.
 *
 */
public class Serializer{	
	
	/**
	 * Lee el mapeo de Films.
	 * @param filename
	 * @return
	 */
	
	public static <K, V> HashMap<K, V> read(String filename) {
		File fichero = new File( filename );
		HashMap<K, V> map = null;
		if(fichero.exists()) {
			try {
				FileInputStream f = new FileInputStream(fichero);
				ObjectInputStream ois = new ObjectInputStream(f);
				map = (HashMap<K, V>) ois.readObject();
				ois.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return map;
	}
	
	/**
	 * Escribe en el archivo con ruta filename el mapeo con films suministrado.
	 * @param map
	 * @param filename
	 */
	
	public static<K, V> void write(HashMap<K, V>map, String filename) {		
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(new FileOutputStream(filename));
			oos.writeObject(map);
			oos.flush();
			oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}