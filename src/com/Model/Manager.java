package com.Model;

import java.util.HashMap;

import org.apache.struts2.ServletActionContext;
/**
 * Clase encargada de Administrar la l�gica del Sistema.
 *
 */
public class Manager {
	
	private HashMap<String,Film> list;
	private String filePath=ServletActionContext.getServletContext().getRealPath("/WEB-INF/films.dat");
	/**
	 * Carga los films.
	 */
	public Manager(){
		list = Serializer.read(filePath);
		if(list==null)
			list = new HashMap<String, Film>();
		Serializer.write(list, filePath);	
	}
	
	/**
	 * Devuelve un iterable con los films.
	 * @return
	 */
	
	public Iterable<Film> getFilms(){
		return list.values();
	}
	
	/**
	 * Agrega un nuevo film.
	 * @param filmBean
	 */
	
	public void add(Film filmBean){
		list = Serializer.read(filePath);
		list.put(filmBean.getTitle(), filmBean);
		Serializer.write(list, filePath);
	}
	/**
	 * Retorna el film correspondiente a la id suministrada.
	 * @param idFilm
	 * @return
	 */
	public Film getFilm(String idFilm){
		return list.get(idFilm);		
	}
	
	/**
	 * Remueve el film correspondiente a la id suministrada.
	 * @param idFilm
	 */
	
	public void remove(String idFilm){
		list = Serializer.read(filePath);
		list.remove(idFilm);
		Serializer.write(list, filePath);
	}	
}
		