<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="stylesheet" type="text/css" href="styles.css"></link>		
				
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
		<title>Agregar</title>
	</head>
	
	<body>
		<s:if test="%{#session.validacionAdmin != true}">
		<% 
			response.sendRedirect("index.jsp");
		%>
		</s:if>
		<div> 
			<center>
				<s:form action="register">		
					<s:if test="film== null">
						<s:textfield name="filmBean.title" label="Título de la película (*)" required="true"  />
			      		<s:textfield name="filmBean.rate"  label ="Calificación 1 a 5 (*)" required="true" />  
			      		<s:textarea name ="filmBean.opinion" required="true" label="Opinión (*)" rows="10" cols="40" wrap="hard"/>
			      		<s:textarea name="filmBean.IMDb" label="IMDb" rows="3" cols="40"  />
			      		<s:textarea name="filmBean.trailer" label="YouTube Trailer (**)" rows="3" cols="40" />
			      		<s:submit value="Listo!" ></s:submit>     	      		
			      	</s:if>
			      	<s:else>
			      		<s:hidden name="oldTitle" value="%{film.title}" />
			      		<s:textfield name="filmBean.title" label="Título de la película (*)" required="true" value="%{film.title}" />
			      		<s:textfield name="filmBean.rate"  label ="Calificación 1 a 5 (*)" required="true" value="%{film.rate}"/>  
			      		<s:textarea name ="filmBean.opinion" required="true" value="%{film.opinion}" label="Opinión (*)" rows="10" cols="40" wrap="hard"/>	      		
			      		<s:textarea name="filmBean.IMDb" label="IMDb" value="%{film.IMDb}"  rows="3" cols="40"/>
			      		<s:textarea name="filmBean.trailer" label="YouTube Trailer (**)" value="%{film.Trailer}"  rows="3" cols="40"/>
			      		<s:submit value="Listo!" ></s:submit>     	    
			      	</s:else>
				</s:form>
					<p>* Campo obligatorio.<br/></p>
					<p>** Tiene que ser de la forma: https://www.youtube.com/watch?v=PulHR2LfPVk.<br/></p>			
			</center>
		</div>
	</body>
</html>