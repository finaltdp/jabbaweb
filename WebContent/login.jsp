<%@ page contentType="text/html; charset=UTF-8"%>
  <%@ taglib prefix="s" uri="/struts-tags" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	
	<head>
		<link rel="stylesheet" type="text/css" href="styles.css"></link>		
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<title>Ingresar</title>
	</head>
	
	<s:if test="%{#session.validacionAdmin != null}">
	<%
		response.sendRedirect("index.jsp");
	%>
	</s:if>	
	
	<body>
		<div>
		<center>
			<s:form action="loginAction" >
				<s:textfield name="username" label="Usuario" required="true" size="20"></s:textfield>
				<s:password name="password" label="Contraseña" required="true" size="21"></s:password>
				<s:submit value="Ingresar" ></s:submit>
			</s:form>
		</center>
		</div>
	</body>
</html>