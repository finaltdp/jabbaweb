<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>		
		<link rel="stylesheet" type="text/css" href="styles.css"></link>
		
		<title>Lista de Películas</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body>
		<div>
			<s:if test="%{#session.validacionAdmin == null}">
				<a href="login.jsp" >Iniciar sesión</a>
			</s:if>
			<s:else>
				<a href="<s:url action='logout' />" >Cerrar sesión</a>		
			</s:else>
			<center>
				<table>
					<tr>
						<th>Película</th>
						<th>Mi puntaje</th>
						<th>Más...</th>
						<s:if test="%{#session.validacionAdmin != null}">
							<th>Editar</th>
							<th>Eliminar</th>
						</s:if>
					</tr>
				
					<s:iterator value="films" var="film">
						<tr>
						  	
							<td><s:property value="title"/></td>
							<td><img src= "stars/<s:property value="rate" />star.png" /></td>
							<td><a href="show?idFilm=<s:property value="title"/>">Más...</a></td>
							<s:if test="%{#session.validacionAdmin != null}">
								<td><a href="edit?idFilm=<s:property value="title"/>">Editar</a></td>
								<td><a href="delete?idFilm=<s:property value="title"/>">Eliminar</a></td>
							</s:if>
						</tr>	
					</s:iterator>
					<tr>
						<s:if test="%{#session.validacionAdmin != null}">
							<td colspan="5"><a href="register.jsp">Agregar una película</a></td>
						</s:if>
					</tr>
				</table>
			</center>
		</div>
	</body>
</html>