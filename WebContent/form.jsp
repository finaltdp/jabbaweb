<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<html>
	<head>	
		<link rel="stylesheet" type="text/css" href="styles.css"></link>		
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><s:property value="Film.title" /> </title>
	</head>
	<body>
		<div>	
			<a href="index.jsp">Volver</a>
			<center>	
				<table id="film-table">					
					<tr>
						<td>Película</td>
						<td><s:property value="Film.title" /></td>
					</tr>
					<tr>
						<td>Puntaje</td>
						<td><img src= "stars/<s:property value="Film.rate" />star.png" /></td>
					</tr>
					<s:if test="Film.IMDb != null">
						<tr>
							<td>IMDb</td>
							<td><a href=<s:property value="Film.IMDb" />>Ficha en IMDb...</a></td>
						</tr>
					</s:if>
				</table>
			</center>
		</div>
		<div>	
			<center>			
				<textarea id="opinion" readonly rows="10" cols="50"><s:property value="Film.opinion" /></textarea>
			</center>
		</div>
		<s:if test="Film.trailer != null">
			<div>
				<center>
					<iframe height="240" width="320" src="${Film.trailer}" allowfullscreen  style="border:none">Trailer en YouTube</iframe>								
				</center>
			</div>
		</s:if>
	</body>
</html>