Herramientas usadas:

-Eclipse Mars. Desde el archivo .war se puede importar el proyecto en el IDE
ya que se incluy� el c�digo fuente en �l.

-Struts 2.3.20. Con las librerias ubicadas en WebContent/WEB-INF/lib.

-Apache Tomcat 8.

-Java 8.

-El sitio demostr� funcionar correctamente tanto en el navegador de Eclipse, 
Google Chrome e Internet Explorer 8.

Usuario y contrase�a: "admin"

Al ingresar a la p�gina se muestra una lista de las peliculas que haya cargadas
(en el archivo films.dat). 
En dicha lista se incluye el t�tulo de la pelicula un puntaje medido entre 1 y
5 estrellas, mas una opci�n "M�s.." que redirecciona a la ficha de una pelicula
en particular donde se muestra adem�s: una breve opini�n, un enlace a IMDb, m�s
un video embebido de youtube (los ultimos dos puede que no dependiendo de si 
fueron agregados por el administrador).
Sobre el borde izquierdo-superior de la pagina de inicio se encuentra la opcion
para loguearse como admin (cuenta �nica). Esto ultimo agrega en la tabla de la
pantalla de inicio las opciones de agregar, editar y eliminar una pelicula de
la lista. Sobre el borde izquierdo-superior de la pagina de inicio se encuentra
ahora la opcion para desloguearse.  

Participantes del proyecto:
Amorosi, Javier (94528)
Antiman, Nehuen (102440)
